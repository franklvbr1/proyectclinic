﻿using CapaEntidades;
using CapaAccesoDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogicaNegocio
{
    public class PacienteLN
    {
        #region "PATRON SINGLETON"
        private static PacienteLN objPaciente = null;
        private PacienteLN() { }
        public static PacienteLN getInstanse()
        {
            if (objPaciente == null)
            {
                objPaciente = new PacienteLN();
            }
            return objPaciente;
        }
        #endregion

        public bool RegistrarPaciente(Paciente objPaciente)
        {
            try
            {
                return PacienteDAO.getInstanse().RegistrarPaciente(objPaciente);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<Paciente> ListarPacientes()
        {
            try
            {
                return PacienteDAO.getInstanse().ListarPacientes();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
