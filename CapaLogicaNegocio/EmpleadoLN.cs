﻿using CapaEntidades;
using CapaAccesoDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogicaNegocio
{
    public class EmpleadoLN
    {
        #region "PATRON SINGLETON"
        private static EmpleadoLN objEmpleado = null;
        private EmpleadoLN()  { }
        public static EmpleadoLN getInstanse()
        {
            if (objEmpleado == null)
            {
                objEmpleado = new EmpleadoLN();
            }
            return objEmpleado;
        }
        #endregion

        public Empleado AccesoSistema(String user, String pass)
        {
            try
            {
                return EmpleadoDao.getInstanse().AccesoSistema(user, pass);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
