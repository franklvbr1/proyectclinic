﻿//import { data } from "./module.js";
//<script type="module" src="main.js"></script>

function addRowDT(data) {
    var tabla = $("#tbl_pacientes").DataTable();
    for (var i = 0; i < tabla.length; i++) {
        tabla.fnAddData ([
            data[i].IdPaciente,
            data[i].Nombres,
            (data[i].ApPaterno + " " + data[i].ApMaterno),
            ((data[i].sexo == 'M')? "Masculino": "Femenino"),
            data[i].Edad,
            data[i].Direccion,
            ((data[i].Estado == true)? "Activo": "Inactivo")
        ]);
    }
}

function sendDataAjax() {
    $.ajax({
        type: "POST",
        url: "GestionarPaciente.aspx/ListarPacientes",
        data: {},
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status + "\n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data) {
            //console.log(data);
            addRowDT(data.d);
        }
    });
}

//llamando a la funcion ajax al ejecutar la página
sendDataAjax();