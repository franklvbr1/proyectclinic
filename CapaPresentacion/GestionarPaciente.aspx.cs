﻿using CapaEntidades;
using CapaLogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CapaPresentacion
{
    public partial class fmrGestionarPaciente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
             
            }
        }

        [WebMethod]
        public static List<Paciente> ListarPacientes()
        {
            List<Paciente> Lista = null;
            try
            {
                Lista = PacienteLN.getInstanse().ListarPacientes();
            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        private Paciente GetEntity()
        {
            Paciente objPaciente = new Paciente();
            objPaciente.IdPaciente = 0;
            objPaciente.Nombres = txtNombre.Text;
            objPaciente.ApPaterno = txtApPaterno.Text;
            objPaciente.ApMaterno = txtApMaterno.Text;
            objPaciente.Edad = Convert.ToInt32(txtEdad.Text);
            objPaciente.Sexo = (ddlSexo.SelectedValue == "Femenino") ? 'F' : 'M';
            objPaciente.NroDocumento = txtNroDocumento.Text;
            objPaciente.Direccion = txtDirección.Text;
            objPaciente.Telefono  = txtTelefono.Text;
            objPaciente.Estado = true;

            return objPaciente;

        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            //Registro del paciente
            Paciente objPaciente = GetEntity();
            //eviar a la capa logica de negocio
            bool response = PacienteLN.getInstanse().RegistrarPaciente(objPaciente);
            if (response == true)
            {
                Response.Write("<script>alert('Registro Correcto')</script>");
            }
            else
            {
                Response.Write("<script>alert('Registro Inorrecto')</script>");
            }
        }
    }
}